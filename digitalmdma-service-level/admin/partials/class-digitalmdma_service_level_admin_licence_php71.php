<?php
# DigitalMDMA - Folgender Code ist Copyright geshützt und darf nicht vervielfäligt oder verändert werden ######## - ANFANG - #########
#
# Author: Florian Buchholz
# Company: DigitalMDMA
# Copyright: 2017
#

#require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/connect.php';

# Kunden-Lizenzen

$current_domain = get_site_url();

#echo $current_domain;

# DigitalMDMA
$licence_customer_digitalmdma_domain_normal = 'https://digitalmdma.com';
$licence_customer_digitalmdma_normal = 'NLICRO-IHRCIX-UXFBTU-VNGVCT';
$licence_customer_digitalmdma_domain_staging = 'https://staging.digitalmdma.com';
$licence_customer_digitalmdma_staging = 'NLICRO-IHRCIX-UXFBTU-VNGVCY';

if($current_domain == $licence_customer_digitalmdma_domain_normal) {
	$current_licence = $licence_customer_digitalmdma_normal;
} else {
	
	if($current_domain == $licence_customer_digitalmdma_domain_staging) {
	$current_licence = $licence_customer_digitalmdma_staging;
	} else {
	
			# Pastor2go
			$licence_customer_pastor2go_domain_normal = 'https://pastor2go.de';
			$licence_customer_pastor2go_normal = 'OEHNGV-IEDHNU-ILGLFX-UKVICA';
			$licence_customer_pastor2go_domain_staging = 'https://staging.pastor2go.de';
			$licence_customer_pastor2go_staging = 'OEHNGV-IEDHNU-ILGLFX-UKVICB';
			
			if($current_domain == $licence_customer_pastor2go_domain_normal) {
				$current_licence = $licence_customer_pastor2go_normal;
			} else {
			
				if($current_domain == $licence_customer_pastor2go_domain_staging) {
					$current_licence = $licence_customer_pastor2go_staging;
				} else {
					
					
						# Natürlich Schlafen
						$licence_customer_natuerlichschlafen_domain_normal = 'https://www.natuerlich-schlafen.de';
						$licence_customer_natuerlichschlafen_normal = 'JFBTRJ-BZIEWI-MPEWPR-ZBZEQC';
						$licence_customer_natuerlichschlafen_domain_staging = 'https://staging.natuerlich-schlafen.de';
						$licence_customer_natuerlichschlafen_staging = 'JFBTRJ-BZIEWI-MPEWPR-ZBZEQD';
						
						if($current_domain == $licence_customer_natuerlichschlafen_domain_normal) {
							$current_licence = $licence_customer_natuerlichschlafen_normal;
						} else {
						
							if($current_domain == $licence_customer_natuerlichschlafen_domain_staging) {
								$current_licence = $licence_customer_natuerlichschlafen_staging;
							} else {
								
									# Vital Salzgrotte
									$licence_customer_vitalsalzgrotte_domain_normal = 'https://vital-salzgrotte.de';
									$licence_customer_vitalsalzgrotte_normal = 'BROXFW-TVZWVF-LJSHEB-ZIDFGW';
									$licence_customer_vitalsalzgrotte_domain_staging = 'https://staging.vital-salzgrotte.de';
									$licence_customer_vitalsalzgrotte_staging = 'BROXFW-TVZWVF-LJSHEB-ZIDFGX';
									
									if($current_domain == $licence_customer_vitalsalzgrotte_domain_normal) {
										$current_licence = $licence_customer_vitalsalzgrotte_normal;
									} else {
									
										if($current_domain == $licence_customer_vitalsalzgrotte_domain_staging) {
											$current_licence = $licence_customer_vitalsalzgrotte_staging;
										} else {
											
												# Das neue Bett
												$licence_customer_dasneuebett_domain_normal = 'https://das-neue-bett.de/wp';
												$licence_customer_dasneuebett_normal = 'WNENTO-CGWTOV-TWCTKF-FSKTHT';
												$licence_customer_dasneuebett_domain_staging = 'https://staging-2-0.server7.digitalmdma.com/wp';
												$licence_customer_dasneuebett_staging = 'WNENTO-CGWTOV-TWCTKF-FSKTHU';
												
												if($current_domain == $licence_customer_dasneuebett_domain_normal) {
													$current_licence = $licence_customer_dasneuebett_normal;
												} else {
												
													if($current_domain == $licence_customer_dasneuebett_domain_staging) {
														$current_licence = $licence_customer_dasneuebett_staging;
													} else {
														
															# Das neue Bett Onlineshop
															$licence_customer_dasneuebettshop_domain_normal = 'https://das-neue-bett.de';
															$licence_customer_dasneuebettshop_normal = 'EMTOBC-GICVSF-FKFHQI-CNFGSK';
															$licence_customer_dasneuebettshop_domain_staging = 'https://staging.das-neue-bett.de';
															$licence_customer_dasneuebettshop_staging = 'EMTOBC-GICVSF-FKFHQI-CNFGSS';
															
															if($current_domain == $licence_customer_dasneuebettshop_domain_normal) {
																$current_licence = $licence_customer_dasneuebettshop_normal;
															} else {
															
																if($current_domain == $licence_customer_dasneuebettshop_domain_staging) {
																	$current_licence = $licence_customer_dasneuebettshop_staging;
																} else {
																		
																		# Welcome Dinner Brausnchweig
																		$licence_customer_wdbs_domain_normal = 'https://www.welcome-dinner-bs.de';
																		$licence_customer_wdbs_normal = 'ISNDUC-BWIFVA-KQVCNV-TOBKDD';
																		$licence_customer_wdbs_domain_staging = 'https://staging.welcome-dinner-bs.de';
																		$licence_customer_wdbs_staging = 'ISNDUC-BWIFVA-KQVCNV-TOBKDE';
																		
																		if($current_domain == $licence_customer_wdbs_domain_normal) {
																			$current_licence = $licence_customer_wdbs_normal;
																		} else {
																		
																			if($current_domain == $licence_customer_wdbs_domain_staging) {
																				$current_licence = $licence_customer_wdbs_staging;
																			} else {
																				
																					# Präventionsrat Salzgitter
																					$licence_customer_praesz_domain_normal = 'https://www.praeventionsrat-salzgitter.de';
																					$licence_customer_praesz_normal = 'PDHVTE-WILUCR-VFTAWX-DZCEUI';
																					$licence_customer_praesz_domain_staging = 'https://staging.praeventionsrat-salzgitter.de';
																					$licence_customer_praesz_staging = 'PDHVTE-WILUCR-VFTAWX-DZCEUJ';
																					
																					if($current_domain == $licence_customer_praesz_domain_normal) {
																						$current_licence = $licence_customer_praesz_normal;
																					} else {
																					
																						if($current_domain == $licence_customer_praesz_domain_staging) {
																							$current_licence = $licence_customer_praesz_staging;
																						} else {
		
		
																								# Yenidze
																								$licence_customer_yenidze_domain_normal = 'https://www.yenidze.eu';
																								$licence_customer_yenidze_normal = 'BILIBR-SUVCWI-EZIRTL-OITTVS';
																								$licence_customer_yenidze_domain_staging = 'https://staging.yenidze.eu';
																								$licence_customer_yenidze_staging = 'BILIBR-SUVCWI-EZIRTL-OITTVT';
																								
																								if($current_domain == $licence_customer_yenidze_domain_normal) {
																									$current_licence = $licence_customer_yenidze_normal;
																								} else {
																								
																									if($current_domain == $licence_customer_yenidze_domain_staging) {
																										$current_licence = $licence_customer_yenidze_staging;
																									} else {
		
																										
																										# KSB
																								$licence_customer_ksb_domain_normal = 'https://ksb-sz.de';
																								$licence_customer_ksb_normal = 'ZRIUPB-INUPTV-ZTVBIM-OOZPMZ';
																								$licence_customer_ksb_domain_staging = 'https://ksb-salzgitter.server7.digitalmdma.com';
																								$licence_customer_ksb_staging = 'ZRIUPB-INUPTV-ZTVBIM-OOZPMA';
																								
																								if($current_domain == $licence_customer_ksb_domain_normal) {
																									$current_licence = $licence_customer_ksb_normal;
																								} else {
																								
																									if($current_domain == $licence_customer_ksb_domain_staging) {
																										$current_licence = $licence_customer_ksb_staging;
																									} else {
		
																								
																								
																												# KGV - Vogelsang e.V. Braunschweig
																												$licence_customer_kgvvsbs_domain_normal = 'https://www.kgv-vogelsang-bs.de';
																												$licence_customer_kgvvsbs_normal = 'LSUBFO-IUWBEF-OWUBEF-IUWBEK';
																												$licence_customer_kgvvsbs_domain_staging = 'https://staging.kgv-vogelsang-bs.de';
																												$licence_customer_kgvvsbs_staging = 'LSUBFO-IUWBEF-OWUBEF-IUWBEW';
																												
																												if($current_domain == $licence_customer_kgvvsbs_domain_normal) {
																													$current_licence = $licence_customer_kgvvsbs_normal;
																												} else {
																												
																													if($current_domain == $licence_customer_kgvvsbs_domain_staging) {
																														$current_licence = $licence_customer_kgvvsbs_staging;
																													} else {
						
																												
																												
																																# BS CONNECT
																																$licence_customer_bsconnectbs_domain_normal = 'https://www.bsconnect.de';
																																$licence_customer_bsconnectbs_normal = 'ZKCNWP-INRVPO-MREIZV-WETCWE';
																																$licence_customer_bsconnectbs_domain_staging = 'https://staging.bsconnect.de';
																																$licence_customer_bsconnectbs_staging = 'ZKCNWP-INRVPO-MREIZV-WETCWF';
																																
																																if($current_domain == $licence_customer_bsconnectbs_domain_normal) {
																																	$current_licence = $licence_customer_bsconnectbs_normal;
																																} else {
																																
																																	if($current_domain == $licence_customer_bsconnectbs_domain_staging) {
																																		$current_licence = $licence_customer_bsconnectbs_staging;
																																	} else {
										
																																
																																
																																																																			# Lina in Berlin
																																$licence_customer_linainberlin_domain_normal = 'https://www.linainberlin.com';
																																$licence_customer_linainberlin_normal = 'ZRILBX-ERIBZR-IBXVIX-VUHMXI';
																																$licence_customer_linainberlin_domain_staging = 'https://staging.linainberlin.com';
																																$licence_customer_linainberlin_staging = 'ZRILBX-ERIBZR-IBXVIX-VUHMXG';
																																
																																
																																if($current_domain == $licence_customer_linainberlin_domain_normal) {
																																	$current_licence = $licence_customer_linainberlin_normal;
																																} else {
																																
																																	if($current_domain == $licence_customer_linainberlin_domain_staging) {
																																		$current_licence = $licence_customer_linainberlin_staging;
																																	} else {
										
																																$current_licence = '000000-000000-000000-000000';
																																
																																	}
																																}

																						}  }}}}}}}}
																					}
																			}
																		}
																}
															}
													}
												}
										}
									}
							}
						}
				}
			}
	}
}
?>