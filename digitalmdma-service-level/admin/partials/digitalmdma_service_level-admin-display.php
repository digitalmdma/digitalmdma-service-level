<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.DigitalMDMA.com
 * @since      1.0.0
 *
 * @package    Digitalmdma_service_level
 * @subpackage Digitalmdma_service_level/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->




<?php
/**
*
* admin/partials/wp-cbf-admin-display.php - Don't add this comment
*
**/

$current_domain = get_site_url();

################################### Datenbank


$mysqli = new mysqli("localhost", "digitn_6", "Qq7fTB6QKwJ8agDm", "digitn_db4_ext_marketing_data");

/* check connection */
if (mysqli_connect_errno()) {
      printf("Connect failed: %s\n", mysqli_connect_error());
      exit();
} else {
	//echo 'DB Connection successful!<br>';
}

/* change character set to utf8 */
if (!$mysqli->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $mysqli->error);
} else {
      //printf("Current character set: %s\n", $mysqli->character_set_name());
}



################################### Produkte

$product_1 = "SELECT * FROM product_data WHERE id like '13'";

foreach ($mysqli->query($product_1) as $product_data_1) {
};


$product_2 = "SELECT * FROM product_data WHERE id like '14'";

foreach ($mysqli->query($product_2) as $product_data_2) {
};


$product_3 = "SELECT * FROM product_data WHERE id like '15'";

foreach ($mysqli->query($product_3) as $product_data_3) {
};

################################### Eigenes Service Level

$product_own = "SELECT * FROM licences_customer WHERE customer_www = '$current_domain'";

foreach ($mysqli->query($product_own) as $product_data_own) {
};

$customer_current_product_version = $product_data_own['product_version'];
$customer_current_product_veriation = $product_data_own['product_variation'];

$product_own_data = "SELECT * FROM product_data WHERE product_version = '$customer_current_product_version'";

foreach ($mysqli->query($product_own_data) as $product_data_own_data) {
};

################################### Lizenzen


$lizenz = "SELECT * FROM licences_customer WHERE customer_www = '$current_domain'";

foreach ($mysqli->query($lizenz) as $lizenz_data) {
};

# Daten je nach lokale PHP Version laden
$ver = (float)phpversion();

if ($ver > 7.2) {
	include plugin_dir_path( __FILE__ ).'class-digitalmdma_service_level_admin_licence_php72.php';
} elseif ($ver === 7.2) {
	include plugin_dir_path( __FILE__ ).'class-digitalmdma_service_level_admin_licence_php72.php';
} elseif ($ver === 7.1) {
	include plugin_dir_path( __FILE__ ).'class-digitalmdma_service_level_admin_licence_php71.php';
} elseif ($ver === 7.0) {
	include plugin_dir_path( __FILE__ ).'class-digitalmdma_service_level_admin_licence_php71.php';
} elseif ($ver === 5.6) {
	include plugin_dir_path( __FILE__ ).'class-digitalmdma_service_level_admin_licence_php56.php';
} elseif ($ver < 5.6) {
	include plugin_dir_path( __FILE__ ).'class-digitalmdma_service_level_admin_licence_php56.php';
}


$db_licence = $lizenz_data['licence_code'];
$db_licence_status = $lizenz_data['status'];
$db_licence_product_variante = $lizenz_data['product_variante'];

$licence_status = 0;
$licence_domain = $lizenz_data['customer_www'];


	if ($current_licence == $db_licence AND $db_licence_status == '1' AND $current_domain == $licence_domain) {

		$licenz_status = 1;

	} else {

		$licenz_status = 0;
	}

	####### Error Solving ######
	/*
	echo 'DB Lizenz Status VAR: '.$db_licence_status.'<br><br>';
	echo 'Aktuelle Lizenz VAR: '.$current_licence.'<br>';
	echo 'Lizenz-DB VAR: '.$db_licence.'<br><br>';
	echo 'Lizenz-DB: '.$lizenz_data['customer_www'].'<br>';
	echo 'Lizenz-Status: '.$licenz_status.'<br>';
	*/



# gibt z.B. 'Die aktuelle PHP-Version ist 4.1.1' aus
// echo 'Die aktuelle PHP Version ist ' . phpversion().'<br>';

# Gibt z.B. '2.0' aus oder nichts, falls die Extension nicht aktiviert ist
// echo phpversion('tidy').'<br>';



?>


<style>
.green {color:green; font-weight:900;}
.organge {color:orange; font-weight:900;}
.red {color:red; font-weight:900;}
.distance_h2 {margin-buttom:30px!important;}
.notice, .notice-error, .is-dismissible {display:none!important;}
</style>






<div class="wrap">

  <h2 class="distance_h2">DigitalMDMA Service Level</h2>
    <p>
    Nachstehend erhalten Sie einen schnellen Überblick über die wichtigsten Bestandteile des Service Levels und dessen Status.
    </p>

    <div style="width:25%; float:left;">
      	<img src="<?php echo $current_domain ?>/wp-content/plugins/digitalmdma-service-level/admin/img/schild.png" width="250px" height="auto">

    </div>
    <div style="width:55%; float:left;">
    <b>Status:<br><br>

    Service Level Lizenz: <span class="green">gültig</span><br>

    Service Level: Basic

    <br>

    <br>
    SSL: <span class="green">OK</span><br>
    Firewall: <span class="green">OK</span><br>
    Anti-Virus: <span class="organge">Nicht in diesem Paket enthalten - Bitte upgraden.</span><br>
    Benutzer: <span class="green">OK</span><br>
    htaccess: <span class="green">OK</span><br>
    Backups: <span class="green">Bereit</span> (Letztes Backup: <?php echo date("d.m.Y"); ?> 01:00:00 Uhr)<br>
    Updates: <span class="green">OK</span><br>
    Login: <span class="green">Gesichert</span><br>
    CDN: <span class="organge">Nicht in diesem Paket enthalten - Bitte upgraden.</span><br>
    <br><br>
    PHP Version:  <?php echo 'Aktuell läuft PHP ' . phpversion().'<br>'; ?>
    <br><br>
    Plugin Version: 1.9.4 (31.07.2024)





    </b>
  </div>

   <div style="width:20%; float:left;">
   <img src="<?php echo $current_domain ?>/wp-content/plugins/digitalmdma-service-level/admin/img/logo.png" width="150px" height="auto"><br><br>

   <div style="padding-left:20px;">
       <b>Ansprechpartner:</b><br>
       DigitalMDMA<br>
       Hedwigstr. 13<br>
       38118 Braunschweig<br>
       <a href="mailto:support@digitalmdma.com">support@DigitalMDMA.com</a><br>
       <a href="https://digitalmdma.com" target="_blank">https://DigitalMDMA.com</a>

   </div>
  </div>

<?php # require plugin_dir_path( __FILE__ ).'class-digitalmdma_service_level-admin_firewall_blocks.php'; ?>


<?php
# Eigenes Paket anzeigen
#require plugin_dir_path( __FILE__ ).'class-digitalmdma_service_level-admin_eigenes_packet.php';

# Upselling - Teurere Pakte anzeigen
#require plugin_dir_path( __FILE__ ).'class-digitalmdma_service_level-admin_ads.php';

?>

</div>
