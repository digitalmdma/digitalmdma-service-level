<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.DigitalMDMA.com
 * @since      1.0.0
 *
 * @package    Digitalmdma_service_level
 * @subpackage Digitalmdma_service_level/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Digitalmdma_service_level
 * @subpackage Digitalmdma_service_level/admin
 * @author     Florian Buchholz <fb@DigitalMDMA.com>
 */



class Digitalmdma_service_level_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Digitalmdma_service_level_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Digitalmdma_service_level_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/digitalmdma_service_level-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Digitalmdma_service_level_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Digitalmdma_service_level_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/digitalmdma_service_level-admin.js', array( 'jquery' ), $this->version, false );

	}






/**
*
* admin/class-wp-cbf-admin.php - Don't add this
*
**/

/**
 * Register the administration menu for this plugin into the WordPress Dashboard menu.
 *
 * @since    1.0.0
 */

public function add_plugin_admin_menu() {

    /*
     * Add a settings page for this plugin to the Settings menu.
     *
     * NOTE:  Alternative menu locations are available via WordPress administration menu functions.
     *
     *        Administration Menus: http://codex.wordpress.org/Administration_Menus
     *
     */


    add_options_page( 'DigitalMDMA™ Service Level ©2024 - Konfiguration', 'Service Level', 'manage_options', $this->plugin_name, array($this, 'display_plugin_setup_page')


    );

/*
	function myplugin_register_settings() {
   add_option( 'myplugin_option_name', 'This is my option value.');
   register_setting( 'myplugin_options_group', 'myplugin_option_name', 'myplugin_callback' );
}
add_action( 'admin_init', 'myplugin_register_settings' );
*/


	function Digitalmdma_service_level_register_settings() {
   add_option( 'licence', 'This is my option value.');
   register_setting( 'Digitalmdma_service_level_group', 'licence', 'Digitalmdma_service_level_callback' );
}
add_action( 'admin_init', 'Digitalmdma_service_level_register_settings' );

}



function profileAddMenu() {
  add_menu_page('Service Level', 'Serivce Level', 10, __FILE__, 'profile');

}








 /**
 * Add settings action link to the plugins page.
 *
 * @since    1.0.0
 */

public function add_action_links( $links ) {
    /*
    *  Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
    */
   $settings_link = array(
    '<a href="' . admin_url( 'admin.php?page=' . $this->plugin_name ) . '">' . __('Einstellungen', $this->plugin_name) . '</a>',
   );
   return array_merge(  $settings_link, $links );

}

/**
 * Render the settings page for this plugin.
 *
 * @since    1.0.0
 */

public function display_plugin_setup_page() {
    include_once( 'partials/digitalmdma_service_level-admin-display.php' );
}



}
