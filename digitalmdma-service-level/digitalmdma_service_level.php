<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.DigitalMDMA.com
 * @since             1.0.0
 * @package           Digitalmdma_service_level
 *
 * @wordpress-plugin
 * Plugin Name:       DigitalMDMA™ Service Level
 * Plugin URI:        http://www.DigitalMDMA.com
 * Description:       Bereitstellung aller Sicherheitsmechanismen des Service Levels von DigitalMDMA™ ©2018.
 * Version:           1.9.3
 * Author:            Florian Buchholz
 * Author URI:        https://www.DigitalMDMA.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       digitalmdma_service_level
 * Domain Path:       /languages
 */

 function run_digitalmdma_service_level_version() {

	$version_of_this_plugin = "1.9.1";
	echo $version_of_this_plugin;

}



 #################################################################
#
#	Dieses Plugin ist urheberrechtlich geschützt.
#	Sie haben keine Verwertungsrechte. Das Kopieren und ein mehrfacher
#	Einsatz dieses durch DigitalMDMA™ geschützen Eigentums ist
#	strengstens untersagt. Zuwiederhandlungen werden durch einen Anwalt
#	umgehened kostenspieleig abgemahnt.
#
#	Alle Rechte liegen bei DigitalMDMA™ ©2018
#
#	www.digitalMDMA.com
#
################################################################

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-digitalmdma_service_level-activator.php
 */
function activate_digitalmdma_service_level() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-digitalmdma_service_level-activator.php';
	Digitalmdma_service_level_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-digitalmdma_service_level-deactivator.php
 */
function deactivate_digitalmdma_service_level() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-digitalmdma_service_level-deactivator.php';
	Digitalmdma_service_level_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_digitalmdma_service_level' );
register_deactivation_hook( __FILE__, 'deactivate_digitalmdma_service_level' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-digitalmdma_service_level.php';


//benutzerdefinierte Weiterleitung nach einem erfolgreichen Login
add_action('wp_login', 'mein_custom_login_redirect_mdma', 10, 2);

function mein_custom_login_redirect_mdma($user_login, $user) {
    // Setzen Sie hier die URL, an die nach dem Login umgeleitet werden soll.
    $redirect_url = '/wp-admin/';

    // Führen Sie die Umleitung aus.
    wp_redirect($redirect_url);
    exit;
}









// Den Zugriff auf die REST API für nicht authentifizierte Benutzer einschränken
add_filter('rest_authentication_errors', function($result) {
    if (!empty($result)) {
        return $result;
    }

    if (!is_user_logged_in()) {
        return new WP_Error('rest_forbidden', 'Nur authentifizierte Benutzer haben Zugriff auf die REST API.', array('status' => 401));
    }

    return $result;
});


// XML-RPC Login Methode deaktivieren
function digitalmdma_disable_xmlrpc_methods($methods) {
    unset($methods['wp.getUsersBlogs']);
    return $methods;
}
add_filter('xmlrpc_methods', 'digitalmdma_disable_xmlrpc_methods');


// XML-RPC vollständig deaktivieren
add_filter('xmlrpc_enabled', '__return_false');

// XML-RPC Methoden deaktivieren
function digitalmdma_disable_xmlrpc_methods_2($methods) {
    return array();
}
add_filter('xmlrpc_methods', 'digitalmdma_disable_xmlrpc_methods_2');

// Funktion zum Hinzufügen des Codes zur .htaccess-Datei
function custom_security_update_htaccess() {
    $htaccess_file = ABSPATH . '.htaccess';
    $rules = "<Files xmlrpc.php>\nOrder Deny,Allow\nDeny from all\n</Files>\n";

    // Überprüfen, ob die .htaccess-Datei beschreibbar ist
    if (is_writable($htaccess_file)) {
        $htaccess_content = file_get_contents($htaccess_file);

        // Überprüfen, ob die Regeln bereits vorhanden sind
        if (strpos($htaccess_content, $rules) === false) {
            // Regeln hinzufügen
            $htaccess_content .= "\n" . $rules;
            file_put_contents($htaccess_file, $htaccess_content);
        }
    } else {
        // Fehlerbehandlung, wenn die .htaccess-Datei nicht beschreibbar ist
        error_log('Die .htaccess-Datei ist nicht beschreibbar.');
    }
}

// Aktion bei der Aktivierung des Plugins ausführen
register_activation_hook(__FILE__, 'custom_security_update_htaccess');

// Aktion bei der Aktualisierung des Plugins ausführen
add_action('upgrader_process_complete', 'custom_security_update_htaccess', 10, 2);

// Aktion, um die .htaccess-Datei bei jedem Laden der Admin-Seite zu überprüfen
add_action('admin_init', 'custom_security_update_htaccess');







/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_digitalmdma_service_level() {

	$plugin = new Digitalmdma_service_level();
	$plugin->run();

}
run_digitalmdma_service_level();
