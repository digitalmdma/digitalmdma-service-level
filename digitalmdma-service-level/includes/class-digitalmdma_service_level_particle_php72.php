<?php
# DigitalMDMA - Folgender Code ist Copyright geshützt und darf nicht vervielfäligt oder verändert werden ######## - ANFANG - #########
#
# Author: Florian Buchholz
# Company: DigitalMDMA
# Copyright: 2017
#

#require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/connect.php';



################################### Branding

#$branding = "SELECT * FROM marketing_digitalmdma WHERE id = '1'";

#foreach ($mysqli->query($branding) as $branding_data) {
#};

#$branding_wordpress_login_screen_img_url = $branding_data['wordpress_login_screen_img_url'];

#$branding_wordpress_login_screen_img_url = 'https://neu.digitalmdma.server7.digitalmdma.com/wp-content/uploads/digitalmdma_wordpress_branding.jpg';

#echo 'Branding: '.$branding_wordpress_login_screen_img_url;

#$branding_wordpress_login_screen_img_url = 'https://neu.digitalmdma.server7.digitalmdma.com/wp-content/uploads/digitalmdma_wordpress_branding.jpg';

$current_domain = get_site_url();

if ($current_domain == 'https://staging-2-0.server7.digitalmdma.com/wp') {

	#echo 'current: '.$current_domain.'<br>';
	#echo 'Server name: '.$_SERVER['SERVER_NAME'].'<br>';
	#echo 'Das neue Bett';

### Login String
function digitalmdma_query_string_protection_for_login_page() {
$QS = "?me=good";
$theRequest = 'https://' . $_SERVER['SERVER_NAME'] . '/wp/' . 'wp-login.php' . '?'. $_SERVER['QUERY_STRING'];

$current_domain = get_site_url();
$new_current_domain = $current_domain.'/wp-login.php'.$QS;

#echo 'Request: '.$theRequest.'<br>';
#echo 'new: '.$new_current_domain;

	if ($new_current_domain == $theRequest ) {
		#echo '<br><br>Funktioniert!';
	} else {
		ob_start();
		#header( 'Location: https://' . $_SERVER['SERVER_NAME'] . '/' );
		#echo '<br><br>Passt nicht!';

		echo'
		<script type="text/javascript">
		window.location.replace("https://'.$_SERVER['SERVER_NAME'].'");
		/* Varianten */
		/* window.document.location.replace("http://www.server.de"); */
		/* document.location.replace("http://www.server.de"); */
		//–>
		</script>
		';

		exit;
	}
}





// Digitalmdma - Login Hintergrund
add_action( 'login_head', 'raw_custom_theme_login_style', 99 );
function raw_custom_theme_login_style() {



 print '<style type="text/css">
html { margin-top:0; }
 body.login{
height:auto;
min-height:100%;
margin-top:0;
padding-top:0px;
padding-bottom:0px;
padding-right:0%;';


// Allgemein
setlocale(LC_TIME, "de");
$tag = date("d"); //Monat
$monat = date("m"); //Monat
$jahr = date("Y");
// Ostern
//echo "Ostern ist am " . strftime("%d.%m.%Y", easter_date($jahr));
$ostermonat = strftime("%m", easter_date($jahr));
$ostertag = strftime("%d", easter_date($jahr));

if ($monat == 12 && $tag < 31)  {
	print 'background: #fff url(/wp-content/plugins/digitalmdma-service-level/assets/images/login-bg-weihnachten.jpg) center center no-repeat;';
	} elseif ($monat == 12 && $tag == 31)  {
		print 'background: #fff url(/wp-content/plugins/digitalmdma-service-level/assets/images/login-bg-silvester.jpg) center center no-repeat;';
	} elseif ($monat == 01 && $tag == 01)  {
		print 'background: #fff url(/wp-content/plugins/digitalmdma-service-level/assets/images/login-bg-neujahr.jpg) center center no-repeat;';
	} elseif ($monat == $ostermonat && $tag == $ostertag || $monat == $ostermonat && $tag == $ostertag+1)  {
		print 'background: #fff url(/wp-content/plugins/digitalmdma-service-level/assets/images/login-bg-ostern.jpg) center center no-repeat;';
	} else {
		print 'background: #fff url(/wp-content/plugins/digitalmdma-service-level/assets/images/login-bg.jpg) center center no-repeat;';
}

print '
background-size:cover; }
.login h1 a { display:none; background: none; }
#loginform { background: #fff; }
.login #nav, .login #backtoblog { text-shadow: none; color: #fff;}
.login #nav a, .login #backtoblog a { color: #fff!important;}
.login #nav a:hover, .login #backtoblog a:hover { color: #2a111e!important;}
</style>'. "\n";
}


} else {

	### Login String
function digitalmdma_query_string_protection_for_login_page() {
$QS = "?me=good";
$theRequest = 'https://' . $_SERVER['SERVER_NAME'] . '/' . 'wp-login.php' . '?'. $_SERVER['QUERY_STRING'];

// these are for testing
#echo $theRequest . '<br>';
#echo site_url('/wp-login.php').$QS.'<br>';
#echo $_SERVER['SERVER_NAME'];

$current_domain = get_site_url();
$new_current_domain = $current_domain.'/wp-login.php'.$QS;

#echo 'Request: '.$theRequest.'<br>';
#echo 'new: '.$new_current_domain;

//Funktion die dafür sorgt dass die Login-Seite nur über eine spezielle Adresse verfügbar ist
if ($new_current_domain == $theRequest ) {
  #echo '<br><br>Funktioniert!';
  #ob_start();
  #header( 'Location: https://' . $_SERVER['SERVER_NAME'] . '/wp-admin/' );
  #echo'
  #<script type="text/javascript">
  #window.location.replace("https://'.$_SERVER['SERVER_NAME'].'/wp-admin/");
  #</script>
  #';
} else {
  ob_start();
  #header( 'Location: https://' . $_SERVER['SERVER_NAME'] . '/' );

	echo'
	<script type="text/javascript">
	window.location.replace("https://'.$_SERVER['SERVER_NAME'].'");
	</script>
	';

  exit;
 }
}

// Digitalmdma - Login Hintergrund
add_action( 'login_head', 'raw_custom_theme_login_style', 99 );
function raw_custom_theme_login_style() {



 print '<style type="text/css">
html { margin-top:0; }
 body.login{
height:auto;
min-height:100%;
margin-top:0;
padding-top:0px;
padding-bottom:0px;
padding-right:0%;';


// Allgemein
setlocale(LC_TIME, "de");
$tag = date("d"); //Monat
$monat = date("m"); //Monat
$jahr = date("Y");
// Ostern
//echo "Ostern ist am " . strftime("%d.%m.%Y", easter_date($jahr));
$ostermonat = strftime("%m", easter_date($jahr));
$ostertag = strftime("%d", easter_date($jahr));

if ($monat == 12 && $tag < 31)  {
	print 'background: #fff url(/wp-content/plugins/digitalmdma-service-level/assets/images/login-bg-weihnachten.jpg) center center no-repeat;';
	} elseif ($monat == 12 && $tag == 31)  {
		print 'background: #fff url(/wp-content/plugins/digitalmdma-service-level/assets/images/login-bg-silvester.jpg) center center no-repeat;';
	} elseif ($monat == 01 && $tag == 01)  {
		print 'background: #fff url(/wp-content/plugins/digitalmdma-service-level/assets/images/login-bg-neujahr.jpg) center center no-repeat;';
	} elseif ($monat == $ostermonat && $tag == $ostertag || $monat == $ostermonat && $tag == $ostertag+1)  {
		print 'background: #fff url(/wp-content/plugins/digitalmdma-service-level/assets/images/login-bg-ostern.jpg) center center no-repeat;';
	} else {
		print 'background: #fff url(/wp-content/plugins/digitalmdma-service-level/assets/images/login-bg.jpg) center center no-repeat;';
}

print '
background-size:cover; }
.login h1 a { display:none; background: none; }
#loginform { background: #fff; }
.login #nav, .login #backtoblog { text-shadow: none; color: #fff;}
.login #nav a, .login #backtoblog a { color: #fff!important;}
.login #nav a:hover, .login #backtoblog a:hover { color: #2a111e!important;}
</style>'. "\n";
}


}

add_action('login_head', 'digitalmdma_query_string_protection_for_login_page');

// remove wordpress version number
  remove_action("wp_head", "wp_generator");

// remove generator from RSS
  function hideversion_returnempty() {
    return "1.0";
  }
  add_filter("the_generator", "hideversion_returnempty");


// Start - DigitalMDMA - Deaktivierung der Passwort-Vergessen-Funktion

function disable_password_reset() { return false; }
add_filter ( 'allow_password_reset', 'disable_password_reset' );

function remove_password_reset_text ( $text ) { if ( $text == 'Passwort vergessen?' ) { $text = ''; } return $text; }

function remove_password_reset() { add_filter( 'gettext', 'remove_password_reset_text' ); }
add_action ( 'login_head', 'remove_password_reset' );

function remove_password_reset_text_in ( $text ) { return str_replace( 'Passwort vergessen?', '', $text ); }
add_filter ( 'login_errors', 'remove_password_reset_text_in');

// ENDE - DigitalMDMA - Deaktivierung der Passwort-Vergessen-Funktion

// Notice in Console usschalten: JQMIGRATE: Migrate is installed, version 1.4.1
add_action('wp_default_scripts', function ($scripts) {
    if (!empty($scripts->registered['jquery'])) {
        $scripts->registered['jquery']->deps = array_diff($scripts->registered['jquery']->deps, ['jquery-migrate']);
    }
});



// ENDE Anpassungen DigitalMDMA


# DigitalMDMA - Obriger Code ist Copyright geshützt und darf nicht vervielfäligt oder verändert werden ######## - ENDE - #########
?>
