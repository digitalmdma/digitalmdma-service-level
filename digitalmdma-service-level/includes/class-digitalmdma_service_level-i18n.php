<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.DigitalMDMA.com
 * @since      1.0.0
 *
 * @package    Digitalmdma_service_level
 * @subpackage Digitalmdma_service_level/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Digitalmdma_service_level
 * @subpackage Digitalmdma_service_level/includes
 * @author     Florian Buchholz <fb@DigitalMDMA.com>
 */
class Digitalmdma_service_level_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'digitalmdma_service_level',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
