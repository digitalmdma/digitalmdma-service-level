<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.DigitalMDMA.com
 * @since      1.0.0
 *
 * @package    Digitalmdma_service_level
 * @subpackage Digitalmdma_service_level/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Digitalmdma_service_level
 * @subpackage Digitalmdma_service_level/includes
 * @author     Florian Buchholz <fb@DigitalMDMA.com>
 */
class Digitalmdma_service_level_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
