<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

// WordPress Bootstrapping
require_once(realpath( dirname( __FILE__ ) ).'/../../../../wp-load.php');

// Sicherstellen, dass das Skript nicht von einem Webbrowser aufgerufen wird
#if (php_sapi_name() !== 'cli') {
#    die("Dieses Skript kann nur über die Kommandozeile ausgeführt werden.");
#}

// WordPress-Core aktualisieren
function update_wordpress_core() {
    include_once ABSPATH . 'wp-admin/includes/admin.php';
    include_once ABSPATH . 'wp-admin/includes/update.php';

    // Prüfen, ob ein Core-Update verfügbar ist
    wp_version_check();
    $updates = get_core_updates();
    if (!empty($updates) && !is_wp_error($updates) && $updates[0]->response == 'upgrade') {
        // WordPress-Core aktualisieren
        $result = wp_update_core($updates[0], ['allow_relaxed_file_ownership' => true]);
        if (is_wp_error($result)) {
            echo "Fehler beim Aktualisieren von WordPress: " . $result->get_error_message() . "\n";
        } else {
            echo "WordPress erfolgreich auf Version " . $updates[0]->current . " aktualisiert.\n";
        }
    } else {
        echo "Keine WordPress-Core-Aktualisierung erforderlich.\n";
    }
}

// Plugins aktualisieren
function update_all_plugins() {
    if (!function_exists('get_plugins')) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    if (!function_exists('wp_update_plugins')) {
        require_once ABSPATH . 'wp-includes/update.php';
    }

    $all_plugins = get_plugins();
    foreach ($all_plugins as $plugin_file => $plugin_info) {
        $update_plugins = get_site_transient('update_plugins');
        if ($update_plugins && isset($update_plugins->response[$plugin_file])) {
            $result = wp_update_plugin($plugin_file);
            if (is_wp_error($result)) {
                echo "Fehler beim Aktualisieren des Plugins: " . $plugin_info['Name'] . "\n";
                echo "Fehlerdetails: " . $result->get_error_message() . "\n";
            } else {
                echo "Plugin erfolgreich aktualisiert: " . $plugin_info['Name'] . "\n";
            }
        }
    }
}

// WordPress und alle Plugins aktualisieren
update_wordpress_core();
update_all_plugins();
?>
